---
title: Contribution à votre projet
---

## De nombreux·ses sympathisant·e·s sont prêt·e·s à contribuer à votre projet !

Identifions ensemble les points où les personnes qui soutiennent votre projet pourront soulager une friction en réalisant une action de taille réduite et ancrée dans leur environnement.

Ce site servira à guider les contributeur·ices dans leurs premiers pas et présentera les missions pour pouvoir réaliser différentes missions, et se coordonner.
